
## based on: https://becominghuman.ai/google-chrome-dinosaur-game-python-bot-b698723e86e
## copyright Taras Rumezhak - https://becominghuman.ai/@rumezhak.taras?source=post_page-----b698723e86e----------------------
# importações (bibliotecas)

from PIL import ImageGrab, ImageOps # coletagem da imagem p/ processamento
import pyautogui # controle do mouse/teclado
import time # delays p/ executar os blocks (ciclo)
import numpy as np #converte imagem p/ array p/ melhor processamento

class Bot:
    """"
    Bot p/ jogar
    """
    def __init__(self):
        self.restart_coords = (480, 503) ## coordenadas botão restart
        self.dino_coords = (207, 534)  ## corodenadas dino qnd começa
        self.area = (self.dino_coords[0] + 90, self.dino_coords[1],
                     self.dino_coords[0] + 150, self.dino_coords[1] + 5) ## coordenadas área onde obstáculos seerão detectados.
    
    def set_dino_coords(self, x, y):
        self.dino_coords = (x, y)

    def set_restart_coords(self, x, y):
    # """
    # Change default restart button coordinates
    # :param x: center x coordinate (int)
    # :param y: center y coordinate (int)
    # :return: None
    # """
        self.restart_coords = (x, y)

    def restart(self):
    # """
    # Restart the game and set default crawl run
    # :return: None
    # """
        pyautogui.click(self.restart_coords)
        pyautogui.keyDown('down')
    

    def jump(self):
        # """
        # Fazer o dino pular
        # :return: None
        # """
        pyautogui.keyUp('down')
        pyautogui.keyDown('space')
        time.sleep(0.095)
        pyautogui.keyUp('space')
        pyautogui.keyDown('down')

    def detection_area(self):
        # """
        # Checa se tem obstáculos em uma determinada área
        # :return: Float
        # """
        image = ImageGrab.grab(self.area)
        gray_img = ImageOps.grayscale(image)
        arr = np.array(gray_img.getcolors())
        # print(arr.mean())
        return arr.mean()
    def main(self):
        # """Loop Principal
        # :return: None
        # """"
        # self.restart()
        while True:
            if self.detection_area() < 273:
                self.jump


bot = Bot()
bot.main()